// El HTML tiene un tag video con ese id
const video = document.getElementById('videito');
// El HTML tiene un tag image con ese id (porque el gif es un formato de imagen)
const preview = document.getElementById('preview');
// Esta variable va a guardar nuestro objeto de grabacion
let recorder;

// Esta funcion abre la camara
const getStream = async () => {
  // video.srcObject es propio del tag video,
  // ahi guardamos el stream que nos devuelve navigator.mediaDevices.getUserMedia
  video.srcObject = await navigator.mediaDevices.getUserMedia({
    // Un objeto de configuracion, donde le decimos al media que elementos vamos a necesitar
    audio: false,
    video: {
      height: {
        max: 480
      },
    },
  });

  // Le damos play al stream
  video.play();
  // Mostramos ese elemnto que antes estaba oculto
  video.classList.remove('hidden');
};

// Esta funcion arranca la grabacion
const startRecord = async () => {
  // Esta funcion nos devuelve el objeto "recorder" y
  // le pasamos el stream que guardamos en que obtuvimos en getStream
  recorder = RecordRTC(video.srcObject, {
    // Objeto de configuracion para comenzar la grabacion
    // El tipo de output que queremos
    type: 'gif',
    // El ratio de frame
    frameRate: 1,
    // La calidad del output
    quality: 10,
    // El alto
    width: 360,
    // Esto no estoy seguro jeje
    hidden: 240,
  });

  // Con el objeto ya creado, emoezamos a grabar
  recorder.startRecording();
};


// Esta funcion finalizala grabacion
const stopRecord = async () => {
  // Paramos la grabacion
  await recorder.stopRecording();
  // Nos guardamos el objeto de la grabacion
  const blob = await recorder.getBlob();
  // Le cargamos una url creada con el objeto URL del DOM
  preview.src = URL.createObjectURL(blob);
  // Ocultamos el video
  video.classList.add('hidden');
  // Mostramos ese elemnto que antes estaba oculto
  preview.classList.remove('hidden');
};